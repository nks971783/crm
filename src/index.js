import React from 'react';
import ReactDOM from 'react-dom';
import './assets/css/style.css';
import App from './App';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { createStore, applyMiddleware  } from 'redux';
import Login from './containers/Login/Login';
import { Provider } from 'react-redux';
import reducer from './store/reducers';
import thunk from 'redux-thunk';

const store = createStore(reducer,applyMiddleware(thunk));

const app =(
<Provider store={store}>
    <Router>
            <Switch>
                <Route path="/login" exact component={Login}/>

                <Route path="/" component={App}/>

            </Switch>
    </Router>
</Provider>
)

ReactDOM.render( app , document.getElementById('root'))
