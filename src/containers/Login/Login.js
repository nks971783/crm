import React, {Component} from 'react';
import GoogleLogin from 'react-google-login';
import { Redirect } from 'react-router-dom';

const responseGoogle = (response) => {
  console.log(response);
  localStorage.setItem('name',response.profileObj.name);
  localStorage.setItem('token',response.Zi.id_token);
  this.setState({'is_login':true});
  return <Redirect to='/'  />
}

class Login extends Component {
  state ={
		is_login: false
	}
  render (){
    let redirect = ''
        if(this.state.is_login){
         	redirect = <Redirect to='/'/>
		    }
    return (
      <div>
        {redirect}
          <h2>Login Page Here</h2>
          <GoogleLogin
            clientId="92783907035-rthd5k5v2b9879gam4j1099hosmnfah9.apps.googleusercontent.com"
            buttonText="Login"
            onSuccess={responseGoogle}
            onFailure={responseGoogle}
            cookiePolicy={'single_host_origin'}
          />
      </div>    
    );
  }
}

export default Login;
