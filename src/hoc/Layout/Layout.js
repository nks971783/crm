import React, {Component} from 'react';
import Header from './../../components/Header/Header';
import Sidebar from './../../components/Sidebar/Sidebar';
import Footer from './../../components/Footer/Footer';

class Layout extends Component {

	render() {
		return (
			<div>
				<Header/>
					<div className="main-wrapper">
						<Sidebar></Sidebar>
						<div className="main-section">
							{this.props.children}
						</div>
					</div>
        		<Footer/>
			</div>
		)
	}
}

export default Layout;
