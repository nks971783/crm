import React, {Component} from 'react';
import Layout from "./hoc/Layout/Layout";
import { Route, Switch, Redirect } from "react-router";
import Leads from './containers/Leads/Leads';
import Dashboard from './containers/Dashboard/Dashboard';
import EmailTemplate from './containers/EmailTemplate/EmailTemplate';

class App extends Component {
  
	render() {
    let redirect = '';
		if(localStorage.getItem('token') === null || localStorage.getItem('token') === ""){

             redirect =  <Redirect to='/login' />

		}
		return (
			<div>
        {redirect}
				<Layout>
					<Switch>
						<Route path="/leads" exact component={Leads} />
						<Route path="/email-template" exact component={EmailTemplate} />
						<Route path="/" exact component={Dashboard} />
					</Switch>
				</Layout>
			</div>
		);
	}
}

export default App;
