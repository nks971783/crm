let initial_state =[]

export default function(state = initial_state, action){
    switch(action.type){
        case 'TEST_ACTION':
             initial_state = action.posts
            return initial_state
        case 'ADD_ACTION':
            let new_state = [...initial_state]
            
            new_state.push(action.post)
            initial_state = new_state
            return initial_state

        default:
    }
    return state; 
}