import {combineReducers} from 'redux';
import BookReducer from './ReducersBooks';
import MovieReducer from './ReducersMovies';
import ActiveMovieReducer from './ReducerActiveMovie';
import CounterReducer from './ReducersCounter';
import PostReducer from './ReducersPosts';
import LoginReducer from './ReducersLoginSignUp';

const rootReduder = combineReducers({
    books: BookReducer,
    movies: MovieReducer,
    active_movie: ActiveMovieReducer,
    count: CounterReducer,
    posts: PostReducer,
    is_login: LoginReducer
})

export default rootReduder;