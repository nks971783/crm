export default function(){
    return [
        { title: 'Movie1', duration: '02:30 Min', url:'https://s3.amazonaws.com/codecademy-content/courses/React/react_video-fast.mp4'},
        { title: 'Movie2', duration: '03:00 Min', url:'https://s3.amazonaws.com/codecademy-content/courses/React/react_video-slow.mp4'},
        { title: 'Movie3', duration: '03:30 Min', url:'https://s3.amazonaws.com/codecademy-content/courses/React/react_video-cute.mp4'},
        { title: 'Movie4', duration: '01:40 Min', url:'https://s3.amazonaws.com/codecademy-content/courses/React/react_video-eek.mp4'},
        { title: 'Movie5', duration: '02:10 Min', url:'https://s3.amazonaws.com/codecademy-content/courses/React/react_video-slow.mp4'}
    ]
}