export function selectMovie(movie){
    return {
        type : 'MOVIE_SELECTED',
        payload: movie
    }
}

export function addCounter(count){
    return {
        type : 'ADD',
        payload: count
    }
}

export function substractionCounter(count){
    return {
        type : 'Substraction',
        payload: count
    }
}

export function isLogin(){
    return {
        type : 'isLogin',
        payload: true
    }
}

export function getPostAction() {  
    return function(dispatch) {
        fetch('https://jsonplaceholder.typicode.com/posts?userId=1')
            .then(response => response.json())
            .then(api_data => {
                dispatch({
                  type: 'TEST_ACTION',
                  posts: api_data
                });
              })
            .catch((error) => {
                console.log(error);
              })
    }
}

export function addPostAction(id, title, body){
    const new_row =  {
                id: id,
                userId: 1,
                title: title,
                body: body
            };
        return {
            type: 'ADD_ACTION',
            post: new_row
        }
        
}