import React, {Component} from 'react';

class Header extends Component {
  render(){
    let name = '';
    if(localStorage.getItem('name') !== null && localStorage.getItem('name') !== ''){
      name = localStorage.getItem('name')
    }
    return (
    
      <header className="App-header">
        <h3>Header {name}</h3>
      </header>
      
  );
  }
}

export default Header;
