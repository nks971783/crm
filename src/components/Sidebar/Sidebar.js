import React from 'react';
import { Link } from 'react-router-dom'

function Sidebar() {
  return (
    
        <div className="left-sidebar">
            <h3>Left Bar</h3>
            <ul>
            <li>
                <Link to="/">Dashboard</Link>
                </li>
                <li>
                <Link to="/leads">Leads</Link>
                </li>
                <li>
                <Link to="/email-template">Email Template</Link>
                </li>
            </ul>
        </div>
        
  );
}

export default Sidebar;
